package panda2;

public class Zad8b
{

	public static void main (String[] args)
	{
		outer: for (int i = 1; i < 3; i++)
		{
			for (int j = 1; j < 4; j++)
			{
				if (j == i)
				{
					System.out.println ("i = " + i + "\tj = " + j + "\t continue...");
					continue outer;

				}
				System.out.println ("i = " + i + "\tj = " + j);
			}
		}
	}
}
