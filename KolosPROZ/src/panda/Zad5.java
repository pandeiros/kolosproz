package panda;

public class Zad5 extends Zad4		// CORRECT ANSWERS: c
{

	public static void main (String[] args)
	{
		Zad3 a = new Zad3 ();
		Zad4 b = new Zad4 ();
		Zad5 c = new Zad5 ();

		System.out.println (a instanceof Zad4);
		System.out.println (b instanceof Zad4);
		System.out.println (c instanceof Zad4);

		/* Result: false true true */

		/**
		 * <pre>
		 * Zad3 a = new Zad4 ();
		 * Zad4 b = new Zad4 ();
		 * Zad5 c = new Zad5 ();
		 * 
		 * System.out.println (a instanceof Zad4);
		 * System.out.println (b instanceof Zad4);
		 * System.out.println (c instanceof Zad4);
		 * </pre>
		 * 
		 * Result: true true true
		 */
	}

}
