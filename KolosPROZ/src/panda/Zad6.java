package panda;

public class Zad6		// CORRECT ANSWERS: b (?)
{

	public static void main (String[] args)
	{
		Tygrys ty;
		Drapiezne pazur;
		Orzel or;
		ty = new Tygrys ();
		pazur = ty;
		or = (Orzel) pazur;
	}
}

class Zwierze
{
}

class Ssak extends Zwierze
{
}

class Ptak extends Zwierze
{
}

interface Drapiezne
{
}

class Tygrys extends Ssak implements Drapiezne
{
}

class Orzel extends Ptak implements Drapiezne
{
}

class Krowa extends Ssak
{
}