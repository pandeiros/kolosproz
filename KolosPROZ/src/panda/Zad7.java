package panda;

public class Zad7		// CORRECT ANSWERS: e
{
	/*
	 * Commented method are incorrect. Method's signature is created from name and list of types of
	 * arguments. Each method must have unique signature, so changing return value, argument name or
	 * adding exception handling does not change anything at all.
	 */

	public float aMethod (float a, float b)
	{
		return 0.0F;
	}

	/*
	 * public float aMethod (float a, float b) { return 0.0F; }
	 */

	/*
	 * public void aMethod (float c, float d) throws Exception { }
	 */

	/*
	 * public int aMethod (float a, float b) { return 0.0F; }
	 */

	/*
	 * public float aMethod (float a, float b) throws Exception { return 0.0F; }
	 */

	private float aMethod (int a, int b, int c)
	{
		return 0.0F;
	}

}
