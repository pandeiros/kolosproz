package panda;

import java.awt.Button;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Zad12 extends Frame implements ActionListener		// CORRECT ANSWER: a, d
{
	public int value;

	private Zad12 (int value)		// For testing events
	{
		System.out.println ("Utworzono obiekt o value = " + value);
		this.value = value;
	}

	private Zad12 (Component cmp)		// For frame creation
	{
		this.setSize (300, 300);
		this.add (cmp);
	}

	private static final long serialVersionUID = 1L;

	public static void main (String[] args)
	{
		Button btn = new Button ("Hello");

		Zad12 a1 = new Zad12 (1);
		Zad12 a2 = new Zad12 (2);
		Zad12 a3 = new Zad12 (3);
		System.out.println ();

		btn.addActionListener (a1);
		btn.addActionListener (a2);
		btn.addActionListener (a3);
		btn.removeActionListener (a2);
		btn.removeActionListener (a3);
		btn.addActionListener (a3);
		btn.addActionListener (a2);

		Zad12 f = new Zad12 (btn);	// Create frame and set button
		f.setVisible (true);		// Show.

	}

	@Override
	public void actionPerformed (ActionEvent e)
	{
		System.out.println ("Zdarzenie przechwycił obiekt o value = " + this.value);
	}
}
