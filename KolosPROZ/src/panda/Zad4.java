package panda;

public class Zad4 extends Zad3		// CORRECT ANSWERS: a, b, c
// Derivation needed for Zad5
{
	public static void main (String[] args)
	{
		System.out.println ("Zadanie 4:");

		/**
		 * Correct
		 * 
		 */
		String x = "XXX";
		int y = 9;
		x += y;
		x = x + "A";

		/**
		 * Correct
		 * 
		 */
		String a = "10";
		int b = 9;
		a = a + b;

		/**
		 * Correct
		 * 
		 */
		String c = null;
		int d = (c != null) && (c.length () > 0) ? c.length () : 0;

		/**
		 * Incorrect. e not initialized
		 * 
		 * <pre>
		 * String e;
		 * int f = 1;
		 * e = e + e;
		 * </pre>
		 */
		String e = "stre ";
		int f = 1;
		e = e + e;

		/**
		 * Incorrect. -= not available.
		 * 
		 * <pre>
		 * String g = &quot;10&quot;;
		 * int h = 9;
		 * g -= h;
		 * </pre>
		 */
		String g = "10";
		int h = 9;
		g += h;

		System.out.println ("");
		System.out.println (x);
		System.out.println (y);
		System.out.println (a);
		System.out.println (b);
		System.out.println (c);
		System.out.println (d);
		System.out.println (e);
		System.out.println (f);
		System.out.println (g);
		System.out.println (h);
	}
}
