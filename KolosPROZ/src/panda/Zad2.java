package panda;

public class Zad2 		// CORRECT ANSWERS: c
{
	public static void main (String[] args)
	{
		System.out.println ("Zadanie 2:");
		compare ();
	}

	static void compare ()
	{
		Zad2 object1 = new Zad2 ();
		Zad2 object2 = new Zad2 ();
		String a = "100.0";
		String b = new String ("100");
		String c = new String ("100");
		int d = 100;
		float e = 100.f;
		String f = "100";
		Integer g = new Integer (100);
		Integer h = new Integer (100);

		System.out.printf ("Value of a:\t%s\n", a);
		System.out.printf ("Value of b:\t%s\n", b);
		System.out.printf ("Value of c:\t%s\n", c);
		System.out.printf ("Value of d:\t%d\n", d);
		System.out.printf ("Value of e:\t%f\n", e);
		System.out.printf ("Value of f:\t%s\n", f);
		System.out.printf ("Value of g:\t%d\n", g);
		System.out.printf ("Value of h:\t%d\n", h);
		System.out.printf ("g.hashCode equals:\t%d\n", g.hashCode ());
		System.out.printf ("h.hashCode equals:\t%d\n\n", h.hashCode ());
		System.out.printf ("Value of object1:\t%s\n", object1);
		System.out.printf ("Value of object2:\t%s\n", object2);
		System.out.printf ("object1.hashCode equals:\t%s\n", object1.hashCode ());
		System.out.printf ("object2.hashCode equals:\t%s\n\n", object2.hashCode ());

		if (a.equals (b)) 		// different string values
			System.out.println ("Comparison 'a.equals (b)' - equal");

		if (b == c) 	// b.equals(c) == true
			System.out.println ("Comparison 'b == c' - equal");

		if (d == e)
			System.out.println ("Comparison 'd == e' - equal");

		if (f == b)		// f.equals(b) == true
			System.out.println ("Comparison 'f == b' - equal");

		if (g == h)		// g.equals (h) == true
			System.out.println ("Comparison 'g == h' - equal");

		if (g.hashCode () == h.hashCode ())
			System.out.println ("Comparison 'g.hashCode () == h.hashCode()' - equal");

		if (object1 == object2)
			System.out.println ("Comparison 'object1 == object2' - equal");

		if (object1.hashCode () == object2.hashCode ())
			System.out.println ("Comparison 'object1.hashCode() == object2.hashCode()' - equal");

		if (object1.equals (object2))
			System.out.println ("Comparison 'object1.equals(object2)' - equal");
	}
}