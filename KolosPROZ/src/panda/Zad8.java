package panda;

public class Zad8		// CORRECT ANSWERS: c (also modifier "final" but only together with "public"
						// (optionally answer b))
{

	// public boolean equals (Object o) CORRECT
	// public final boolean equals (Object o)
	//
	// ______ boolean equals (Object o) INCORRECT
	// protected boolean equals (Object o)
	// final boolean equals (Object o)
	// static boolean equals (Object o)
	// abstract boolean equals (Object o)
	//
	// Object.equals(Object o) is a public method. Therefore reducing visibility with above keywords
	// is incorrect. Additionally 'static' method would hide instance method.

	public boolean equals (Object o)
	{
		return true;
	}

}
