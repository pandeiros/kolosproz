package panda;

public class Zad9		// CORRECT ANSWERS: a
{
	public static void main (String[] args)
	{
		Object x[] = new Integer[25];
		int y[] = new int[25];

		System.out.println (x);
		System.out.println (x[0]);
		System.out.println (x[24]);
		// System.out.println (x[25]); Throws Exception: ArrayOutOfBounds

		System.out.println ();
		System.out.println (y);
		System.out.println (y[0]);
		System.out.println (y[24]);
		// System.out.println (y[25]); Throws Exception: ArrayOutOfBounds

	}
}
