package panda;

import java.io.FileInputStream;
import java.io.IOException; // poziom 2
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.net.MalformedURLException; // poziom 3
import java.net.URL;

public class Zad1		// CORRECT ANSWERS: e, d
{

	public static void main (String[] args)
	{
		System.out.println ("Zadanie 1:");
		try
		{
			String s = new String ("sFKlfkalgknalgknalgkn");
			URL u = new URL (s); // s zdefiniowany wcześniej
			ObjectInputStream in = new ObjectInputStream (new FileInputStream ("plik.txt"));
			Object o = in.readObject (); // in jest odnośnikiem do ObjectInputStream
			System.out.println ("Sukces");
		}
		catch (MalformedURLException e)
		{
			System.out.println ("Zły URL");
		}
		catch (StreamCorruptedException e)
		{
			System.out.println ("Nieprawidłowa zawartość");
		}
		catch (IOException e)
		{
			System.out.println ("Złapany wyjątek we/wy");
		}
		catch (Exception e)
		{
			System.out.println ("Złapany wyjątek ogólny");
		}
		finally
		{
			System.out.println ("Wykonanie bloku finally");
		}
		System.out.println ("Kontynuacja...");
	}

}
