package panda;

public class Zad19		// CORRECT ANSWERS: c, e
{

	public static void main (String[] args)
	{
		int i = 0, j = 1;

		System.out.println ("Example a:");
		if ((i == 0) || (j / i == 1))		// no exception
			;

		System.out.println ("Example b:");	// no exception
		if ((i != 0) | (j / (double) i == 1))
			;

		System.out.println ("Example c:"); // exception: Division by zero
		if ((i != 0) & (j / i == 1))
			;

		System.out.println ("Example d:");	  // no exception
		if ((i != 0) & (j / (double) i == 1))
			;

		System.out.println ("Example e:");	  // exception: Division by zero
		if ((i == 0) | (j / i == 1))
			;
	}
}
