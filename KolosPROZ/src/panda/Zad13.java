package panda;

public class Zad13		// CORRECT ANSWERS: b, e
{
	public class InClassPublic
	{

	}

	// Derivation acceptable. >>> Answer a incorrect <<<
	public class InClassPublicDerived extends InClassPublic
	{

	}

	// Private modifier acceptable. >>> Answer b correct <<<
	private class InClassPrivate
	{

	}

	public static void main (String[] args)
	{
		int local = 0;
		final int flocal = 0;

		// Class has access to method's variables, but only final ones. >>> Answer c incorrect <<<
		final class InClassInMethod
		{
			public InClassInMethod ()
			{
				// System.out.println (local); Incorrect
				System.out.println (flocal);

				// Class within a class declaration acceptable. >>> Answer d incorrect <<<
				class InClassInClass
				{

				}
			}
		}

		// Cannot instantiate inner class without outer class instantiation >>> Answer e correct <<<

		// InClassPrivate icPrivate = new InClassPrivate (); INCORRECT
	}

}
