package panda;

public class Zad20		// CORRECT ANSWER: b
{

	public static void main (String[] args)
	{
		// Available conversions: int, long, float, double.
		byte b = 5;
		short s = 3;
		// byte result1 = b * ++s;
		// char result2 = b * ++s;
		// short result3 = b * ++s;
		int result4 = b * ++s;
		long result5 = b * ++s;
		float result6 = b * ++s;
		double result7 = b * ++s;
		// boolean result8 = b * ++s;

	}

}
