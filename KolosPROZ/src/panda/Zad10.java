package panda;

import java.awt.Button;
import java.awt.Frame;

public class Zad10 extends Frame		// CORRECT ANSWERS: a, d
{
	private Zad10 ()
	{
		setSize (300, 300);
		Button b = new Button ("Apply");
		add (b);
	}

	public static void main (String args[])
	{
		Zad10 that = new Zad10 ();

		that.setVisible (true);
	}
}