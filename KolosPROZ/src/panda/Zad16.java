package panda;

public class Zad16 extends Zad16BaseOne		// CORRECT ANSWERS: d
{
	public void test ()
	{
		System.out.println ("Metoda klasy Zad16");
	}

	public static void main (String[] args)
	{
		Zad16 object = new Zad16 ();
		object.init ();

	}

	public void init ()
	{
		super.test ();		// Calls Zad16BaseOne method. >>> Answer a incorrect <<<
		test ();		// Obviously calls Zad16 method. >>> Answer b incorrect <<<
		// ::test(); Doesn't work. >>> Answer c incorrect <<<
		// C.test(); Incorrect, C.test() is non-static. >>> Answer e incorrect <<<

		// Only answer d is correct, you cannot call a method from "two levels above".
	}

}

class Zad16BaseOne extends Zad16BaseTwo
{
	public void test ()
	{
		System.out.println ("Metoda klasy Zad16BaseOne");
	}
}

class Zad16BaseTwo
{
	public void test ()
	{
		System.out.println ("Metoda klasy Zad16BaseTwo");
	}
}
