package panda;

public class Zad15		// CORRECT ANSWER: a
{

	public static void main (String[] args)
	{
		String s = "abcde";					// 1
		String s1 = new String ("abcde"); 	// 2.
		if (s == s1)						// 3.
			s1 = null;						// 4.
		else if (s1.equals (s))				// 5.
			s = null;						// 6.

		System.out.println (s + "\n" + s1);
	}
}
