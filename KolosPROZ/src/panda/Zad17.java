package panda;

abstract class Zad17Primo		// CORRECT ANSWERS: c, d
{
	protected/* final */abstract double d ();		// Only in abstract class, but without final
	// a - incorrect
}

abstract class Zad17Secundo
{

	abstract/* final */double hypCos ();		// Only in abstract class, but without final
	// b - incorrect
}

public class Zad17
{
	protected final static native Integer method ();	// c - correct

	transient volatile static int i = 41;	// d - correct

	// static i = 1; e - incorrect, missing type specifier
}
