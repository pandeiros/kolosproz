package panda;

public class Zad3		// CORRECT ANSWERS: d
{
	public static void main (String args[])
	{
		System.out.println ("Zadanie 3:");
		String str1 = "abc";
		String str2 = "def";
		String str3 = str1.concat (str2);
		str1.concat (str2);

		System.out.println ("Wypisz str1, str2 oraz str3");
		System.out.println (str1);
		System.out.println (str2);
		System.out.println (str3);

		System.out.printf ("\nZmienna str1 nie zmienila sie, poniewaz funkcja metoda concat() \n"
		        + "nie modyfikuje obiektu ale zwraca nowy lancuch");
	}
}