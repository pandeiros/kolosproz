package panda;

public class Zad14		// CORRECT ANSWERS: a
{

	public static void main (String[] args)
	{

		int[] c[];		// a - correct

		// int d[5][5]; // b, c, d, e - incorrect
		// int a[5][];
		// int a[-1][];
		// int d[5][-5];

		/*
		 * Array size is only declared in initialization. Declaration only need brackets for array
		 * indication.
		 */
	}
}
